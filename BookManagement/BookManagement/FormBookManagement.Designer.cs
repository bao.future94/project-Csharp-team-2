﻿namespace BookManagement
{
    partial class FormBookManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBookManagement));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPublisher = new System.Windows.Forms.Label();
            this.cbPublisher = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.isbnTxtbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.authorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AddBttn = new System.Windows.Forms.Button();
            this.bookManagementDataSet = new BookManagement.BookManagementDataSet();
            this.searchTextbox = new System.Windows.Forms.TextBox();
            this.searchBttn = new System.Windows.Forms.Button();
            this.refreshBttn = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookManagementDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 274);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(534, 182);
            this.dataGridView1.TabIndex = 0;
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(87, 164);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(181, 20);
            this.txtTitle.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Title";
            // 
            // lblPublisher
            // 
            this.lblPublisher.AutoSize = true;
            this.lblPublisher.Location = new System.Drawing.Point(12, 239);
            this.lblPublisher.Name = "lblPublisher";
            this.lblPublisher.Size = new System.Drawing.Size(50, 13);
            this.lblPublisher.TabIndex = 4;
            this.lblPublisher.Text = "Publisher";
            // 
            // cbPublisher
            // 
            this.cbPublisher.DisplayMember = "name";
            this.cbPublisher.FormattingEnabled = true;
            this.cbPublisher.Location = new System.Drawing.Point(87, 231);
            this.cbPublisher.Name = "cbPublisher";
            this.cbPublisher.Size = new System.Drawing.Size(181, 21);
            this.cbPublisher.TabIndex = 5;
            this.cbPublisher.ValueMember = "id";
            // 
            // btnUpdate
            // 
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnUpdate.FlatAppearance.BorderSize = 0;
            this.btnUpdate.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.btnUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(570, 336);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 44);
            this.btnUpdate.TabIndex = 7;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDel
            // 
            this.btnDel.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnDel.FlatAppearance.BorderSize = 0;
            this.btnDel.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.btnDel.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDel.Image = global::BookManagement.Properties.Resources.delete;
            this.btnDel.Location = new System.Drawing.Point(570, 404);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(75, 44);
            this.btnDel.TabIndex = 8;
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // isbnTxtbox
            // 
            this.isbnTxtbox.Location = new System.Drawing.Point(87, 102);
            this.isbnTxtbox.Name = "isbnTxtbox";
            this.isbnTxtbox.Size = new System.Drawing.Size(181, 20);
            this.isbnTxtbox.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "ISBN";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(290, 102);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(256, 150);
            this.dataGridView2.TabIndex = 12;
            // 
            // authorsBindingSource
            // 
            this.authorsBindingSource.DataMember = "Authors";
            // 
            // AddBttn
            // 
            this.AddBttn.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.AddBttn.FlatAppearance.BorderSize = 0;
            this.AddBttn.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.AddBttn.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.AddBttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddBttn.Image = global::BookManagement.Properties.Resources.add;
            this.AddBttn.Location = new System.Drawing.Point(570, 274);
            this.AddBttn.Name = "AddBttn";
            this.AddBttn.Size = new System.Drawing.Size(75, 44);
            this.AddBttn.TabIndex = 14;
            this.AddBttn.UseVisualStyleBackColor = true;
            this.AddBttn.Click += new System.EventHandler(this.AddBttn_Click);
            // 
            // bookManagementDataSet
            // 
            this.bookManagementDataSet.DataSetName = "BookManagementDataSet";
            this.bookManagementDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // searchTextbox
            // 
            this.searchTextbox.Location = new System.Drawing.Point(87, 41);
            this.searchTextbox.Name = "searchTextbox";
            this.searchTextbox.Size = new System.Drawing.Size(459, 20);
            this.searchTextbox.TabIndex = 15;
            // 
            // searchBttn
            // 
            this.searchBttn.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.searchBttn.FlatAppearance.BorderSize = 0;
            this.searchBttn.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.searchBttn.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.searchBttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchBttn.Image = ((System.Drawing.Image)(resources.GetObject("searchBttn.Image")));
            this.searchBttn.Location = new System.Drawing.Point(552, 34);
            this.searchBttn.Name = "searchBttn";
            this.searchBttn.Size = new System.Drawing.Size(75, 33);
            this.searchBttn.TabIndex = 16;
            this.searchBttn.UseVisualStyleBackColor = true;
            this.searchBttn.Click += new System.EventHandler(this.searchBttn_Click);
            // 
            // refreshBttn
            // 
            this.refreshBttn.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.refreshBttn.FlatAppearance.BorderSize = 0;
            this.refreshBttn.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.refreshBttn.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.refreshBttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.refreshBttn.Image = ((System.Drawing.Image)(resources.GetObject("refreshBttn.Image")));
            this.refreshBttn.Location = new System.Drawing.Point(570, 154);
            this.refreshBttn.Name = "refreshBttn";
            this.refreshBttn.Size = new System.Drawing.Size(75, 42);
            this.refreshBttn.TabIndex = 17;
            this.refreshBttn.UseVisualStyleBackColor = true;
            this.refreshBttn.Click += new System.EventHandler(this.refreshBttn_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // FormBookManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 468);
            this.Controls.Add(this.refreshBttn);
            this.Controls.Add(this.searchBttn);
            this.Controls.Add(this.searchTextbox);
            this.Controls.Add(this.AddBttn);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.isbnTxtbox);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.cbPublisher);
            this.Controls.Add(this.lblPublisher);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.Name = "FormBookManagement";
            this.Text = "Book Management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormBookManagement_FormClosing);
            this.Load += new System.EventHandler(this.FormBookManagement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookManagementDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPublisher;
        private System.Windows.Forms.ComboBox cbPublisher;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDel;
        private BookManagementDataSet bookManagementDataSet;
        //private System.Windows.Forms.BindingSource publishersBindingSource;
        //private BookManagementDataSetTableAdapters.PublishersTableAdapter publishersTableAdapter;
        //private BookManagementDataSet1 bookManagementDataSet1;
        private System.Windows.Forms.BindingSource authorsBindingSource;
        //private BookManagementDataSet1TableAdapters.AuthorsTableAdapter authorsTableAdapter;
        private System.Windows.Forms.TextBox isbnTxtbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView2;
        //private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
       // private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button AddBttn;
        private System.Windows.Forms.TextBox searchTextbox;
        private System.Windows.Forms.Button searchBttn;
        private System.Windows.Forms.Button refreshBttn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}

