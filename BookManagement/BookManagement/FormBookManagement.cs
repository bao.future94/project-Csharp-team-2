﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class FormBookManagement : Form
    {
        private static FormBookManagement Instance = null;
        BookManagementEntities bookEntities = new BookManagementEntities();

        private FormBookManagement()
        {
            InitializeComponent();

            this.load_grid();
            this.load_author_grid();
            load_publisher_combobox();
        }

        public static FormBookManagement GetInstance()
        {
            if(Instance == null)
            {
                Instance = new FormBookManagement();
            }
            return Instance;
        }

        private void btnAddAuthor_Click(object sender, EventArgs e)
        {
        }

        private void FormBookManagement_Load(object sender, EventArgs e)
        {
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            FormBookUpdate myForm = new FormBookUpdate();
            myForm.Show();
            this.Hide();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
            {
                Book temp = this.RemoveReferences(row.Cells[0].Value.ToString());
                if (temp != null)
                    this.bookEntities.Books.Remove(temp);
                else
                {
                    MessageBox.Show("Deleting Error !");
                    return;
                }
                bookEntities.SaveChanges();
            }
            MessageBox.Show("Deleting Successfully !");
            this.load_grid();
            FormHireBook.GetInstance().FillData();
        }

        private void load_grid()
        {
            var result = from Book in bookEntities.Books
                         from Author in Book.Authors
                         select new
                         {
                             ISBN = Book.isbn,
                             Title = Book.title,
                             Publisher = Book.Publisher.name,
                             Author = Author.name,
                             //Price = Book.price
                         };

            dataGridView1.DataSource = result.ToList();
        }

        public void load_author_grid()
        {
            var result = from Author in this.bookEntities.Authors
                         select new
                         {
                            ID = Author.id,
                            Name = Author.name
                         };
            dataGridView2.DataSource = result.ToList();
        }

        public void load_publisher_combobox()
        {
            List<Publisher> publisher = bookEntities.Publishers.ToList();
            cbPublisher.DataSource = publisher;
            cbPublisher.DisplayMember = "name";
            cbPublisher.ValueMember = "id";
        }

        private Boolean searchText(String text)
        {
            Boolean success = false;

            var result = (from Book in bookEntities.Books
                         from Author in Book.Authors
                         select new
                         {
                             ISBN = Book.isbn,
                             Title = Book.title,
                             Publisher = Book.Publisher.name,
                             Author = Author.name,
                         }).Where(record =>record.Title.Contains(text) ||
                                  record.Publisher.Contains(text) || record.Author.Contains(text));
            dataGridView1.DataSource = result.ToList();

            if (result.Count() != 0)
                success = true;

            return success;
        }
        private Book AddBook()
        {
            Book newBook = new Book();
            newBook.isbn = this.isbnTxtbox.Text;
            newBook.title = this.txtTitle.Text;
            newBook.publisher_id = (int)this.cbPublisher.SelectedValue;
            newBook.Publisher = this.bookEntities.Publishers.Find(newBook.publisher_id);
            
            foreach (DataGridViewRow row in this.dataGridView2.SelectedRows)
            {
                Author a = this.bookEntities.Authors.Find(int.Parse(row.Cells[0].Value.ToString()));
                newBook.Authors.Add(a);
            }

            if (newBook.Authors.Count == 0)
                newBook = null;

            return newBook;
        }

        private Book RemoveReferences(String id)
        {
            Book b = this.bookEntities.Books.Find(id);

            // remove book's authors
            if (b != null)
            {
                foreach (var author in b.Authors.ToList())
                {
                    if (!b.Authors.Remove(author))
                    {
                        b = null;
                        break;
                    }                     
                }

                foreach (var order in b.Orders.ToList())
                {
                    if (!b.Orders.Remove(order))
                    {
                        b = null;
                        break;
                    }
                }
            }

            return b;
        }

        private int CheckUnfilledAndExistingData()
        {
            int result = 0;
            if (this.isbnTxtbox.Text.Equals(""))
                result = 1;
            else if (this.txtTitle.Text.Equals(""))
                result = 2;
            else if (this.bookEntities.Books.Find(this.isbnTxtbox.Text) != null)
                result = 3;
            return result;
        }

        private void AddBttn_Click(object sender, EventArgs e)
        {
            this.searchTextbox.Text = "";
            int result = this.CheckUnfilledAndExistingData();

            if (result == 1)
            {
                MessageBox.Show("You have to fill book's isbn !");
                return;
            }
            else if (result == 2)
            {
                MessageBox.Show("You have to fill book's title !");
                return;
            }
            else if (result == 3)
            {
                MessageBox.Show("The book with this isbn exists, Please fill another!");
                return;
            }

            Book newBook = this.AddBook();
            
            if (newBook == null)
            {
                MessageBox.Show("You have to choose authors !");
                return;
            }

            this.bookEntities.Books.Add(newBook);
            this.bookEntities.SaveChanges();
            this.load_grid();
            MessageBox.Show("Insert success");
            FormHireBook.GetInstance().FillData();
        }

        private void FormBookManagement_FormClosing(object sender, FormClosingEventArgs e)
        {         
            this.Dispose();
            this.Close();
        }

        private void searchBttn_Click(object sender, EventArgs e)
        {
            this.AddBttn.Enabled = false;
            this.btnDel.Enabled = false;
            this.isbnTxtbox.Text = "";
            this.txtTitle.Text = "";

            if (!this.searchText(this.searchTextbox.Text))
                MessageBox.Show("No results found!");
        }

        private void refreshBttn_Click(object sender, EventArgs e)
        {
            this.AddBttn.Enabled = true;
            this.btnDel.Enabled = true;
            this.isbnTxtbox.Text = "";
            this.txtTitle.Text = "";
            this.load_grid();
        }

       

        
    }
}
