﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity.Validation;

namespace BookManagement
{
    public partial class FormPublisherManagement : Form
    {
        private static FormPublisherManagement Instance = null;
        private FormPublisherManagement()
        {
            InitializeComponent();
           
        }

        public static FormPublisherManagement GetInstance()
        {
            if (Instance == null)
            {
                Instance = new FormPublisherManagement();
            }
            return Instance;
         }

        BookManagementEntities be = new BookManagementEntities();
        /*
        * Method Select
        */
        private void FillData()
        {
            dataGridView2.DataSource = be.Publishers.Select(p => new
            {
                id = p.id,
                name = p.name
            }).OrderByDescending(p => p.id).ToList();
            dataGridView2.Columns[0].HeaderText = "ID";
            dataGridView2.Columns[1].HeaderText = "Publisher Name";
        }

        /*
        *  
        */
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int publisherId = int.Parse(this.dataGridView2.CurrentRow.Cells[0].Value.ToString());
            Publisher publishers = be.Publishers.Single(publisher => publisher.id.Equals(publisherId));
            this.textBox1.Text = publishers.name;
        }


        /*
         * Method Update
         */
        private void UpdatePublisher()
        {
            be = new BookManagementEntities();
            int publisherId = int.Parse(this.dataGridView2.CurrentRow.Cells[0].Value.ToString());
            Publisher publisher = be.Publishers.Where(p => p.id == publisherId).SingleOrDefault();
            if (publisher != null)
            {
                publisher.name = textBox1.Text;
                be.SaveChanges();
                this.textBox1.Clear();
            }
            FillData();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdatePublisher();
            FormBookManagement.GetInstance().load_publisher_combobox();
        }

         /*
        * Form Load
        */
        private void FormPublisherManagement_Load(object sender, EventArgs e)
        {
            FillData();
            dataGridView2.Columns["Id"].Width = 50;
            dataGridView2.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        /*
        * Method Add
        */
        private void Add()
        {
            be = new BookManagementEntities();
            var p = new Publisher();
           
            if(textBox1.Text == "")
            {
                MessageBox.Show("Pls input name");
            }
            else
            {
                p.name = textBox1.Text;
                be.Publishers.Add(p);
                be.SaveChanges();
                this.textBox1.Clear();
            }
            FillData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Add();
            FormBookManagement.GetInstance().load_publisher_combobox();
        }

        /*
         * Method Delete
        */
        private void Delete()
        {
           
                int publisherId = int.Parse(this.dataGridView2.CurrentRow.Cells[0].Value.ToString());
                Publisher publisher = be.Publishers.Single(publishers => publishers.id.Equals(publisherId));
                DialogResult dialogResult = MessageBox.Show("Are you sure", "Confirm", MessageBoxButtons.YesNo);
            if (publisher != null && dialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    be.Publishers.Remove(publisher);
                    this.textBox1.Clear();
                    be.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("Foreing Key");
                }

            }
            
           
           
            FillData();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            Delete();
            FormBookManagement.GetInstance().load_publisher_combobox();
        }

        /*
         * Method Search
         */
        private void Search()
        {
            if (textBox1.Text.Length > 0)
            {
                string search = textBox1.Text;
                var name = be.Publishers.FirstOrDefault(p => p.name.Contains(search));
                if (search == null)
                {
                    MessageBox.Show("Name not found");
                    FillData();
                }
                else
                {
                    dataGridView2.DataSource = be.Publishers.Where(pu => pu.name.Contains(textBox1.Text)).Select(p => new
                    {
                        id = p.id,
                        name = p.name
                    }).ToList();
                }
                
            }
            
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }
        
        /*
         * Clear textbox and reload data
         */
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(textBox1.Text == "")
            {
                FillData();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormBookManagement.GetInstance().Show();
           
        }
    }
}
