﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class FormAccountManagement : Form
    {
        private static FormAccountManagement Instance = null;
        private FormAccountManagement()
        {
            InitializeComponent();
        }
        public static FormAccountManagement GetInstance()
        {
            if (Instance ==null)
            {
                Instance = new FormAccountManagement();
            }
            return Instance;
        }

        BookManagementEntities be = new BookManagementEntities();

        private void FillData()
        {
            dataGridView1.DataSource = be.Accounts.Select(a => new {
                username = a.username,
                password = a.password,
                fullname = a.fullname,
                role = a.role
            }).OrderByDescending(a => a.username).ToList();

            dataGridView1.Columns[0].HeaderText = "Username";
            dataGridView1.Columns[1].HeaderText = "Password";
            dataGridView1.Columns[2].HeaderText = "Fullname";
            dataGridView1.Columns[3].HeaderText = "Role";
            dataGridView1.Columns["Username"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["Password"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["Fullname"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["Role"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        
        private void AddAccount()
        {
            var a = new Account();

           
            if (String.IsNullOrEmpty(txtUsername.Text) || String.IsNullOrEmpty(txtPassword.Text) || String.IsNullOrEmpty(txtFullname.Text) || String.IsNullOrEmpty(cbRole.Text))
            {
                MessageBox.Show("Pls input full field");
            }
            else
            {
                a.username = txtUsername.Text;
                a.password = txtPassword.Text;
                a.fullname = txtFullname.Text;
                a.role = cbRole.Text;
                be.Accounts.Add(a);
                be.SaveChanges();
            }
            this.txtUsername.Clear();
            this.txtPassword.Clear();
            this.txtFullname.Clear();

            FillData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddAccount();
            FormHireBook.GetInstance().LoadComboBox();
        }

        private void FormAccountManagement_Load(object sender, EventArgs e)
        {
            FillData();
            cbRole.Items.Add("ROLE_ADMIN");
            cbRole.Items.Add("ROLE_USER");
        }
        
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string accountId = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
            Account accounts = be.Accounts.Single(account => account.username.Equals(accountId));
            this.txtUsername.Text = accounts.username;
            this.txtPassword.Text = accounts.password;
            this.txtFullname.Text = accounts.fullname;
            this.cbRole.Text = accounts.role;
        }

        /*
         * Method Delete
        */
        private void DeleteAccount()
        {
            string accountId = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
            Account account = be.Accounts.Single(accounts => accounts.username.Equals(accountId));
            DialogResult dialogResult = MessageBox.Show("Are you sure", "Confirm", MessageBoxButtons.YesNo);
            if (account != null && dialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                be.Accounts.Remove(account);
                this.txtUsername.Clear();
                this.txtPassword.Clear();
                this.txtFullname.Clear();
                be.SaveChanges();
            }
            FillData();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteAccount();
            FormHireBook.GetInstance().LoadComboBox();
        }
        
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormHireBook.GetInstance().LoadComboBox();
            FormHireBook.GetInstance().Show();
        }
    }
}
