﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class FormOrderDetails : Form
    {
        private static FormOrderDetails Instance = null;



        BookManagementEntities bme = new BookManagementEntities();
        private FormOrderDetails()
        {
            InitializeComponent();
        }
        public static FormOrderDetails GetInstance()
        {
            if (Instance == null)
            {
                Instance = new FormOrderDetails();

            }
            return Instance;
        }


        private void FillOrder()
        {
            this.dataGridView1.DataSource = bme.Orders.Select(o => new
            {
                ID = o.id,
                Title = o.title,
                Username = o.username,
                CreatedDate = o.createddate,
                ReturnDate = o.returndate
            }).OrderByDescending(order => order.ID).ToList();
        }



        private void FormOrderDetails_Load(object sender, EventArgs e)
        {
            FillOrder();

        }

        public FormOrderDetails LoadData()
        {
            FillOrder();
            return this;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = int.Parse(this.dataGridView1.CurrentRow.Cells[0].Value.ToString());
            var query = from Book in bme.Books
                        where Book.Orders.Any(o => o.id == id)
                        select Book;
            dataGridView2.DataSource = query.ToList();
            SetComlumnFalse();
        }

        private void SetComlumnFalse()
        {
            if (dataGridView2.DataSource != null)
            {
                dataGridView2.Columns[2].Visible = false;
                dataGridView2.Columns[3].Visible = false;
                dataGridView2.Columns[4].Visible = false;
                dataGridView2.Columns[5].Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormHireBook.GetInstance().LoadData().Show();
        }
    }
}
