﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class FormAuthorManagement : Form
    {

        private static FormAuthorManagement Instance = null;
        private FormAuthorManagement()
        {
            InitializeComponent();
        }

        public static FormAuthorManagement GetInstance()
        {
            if (Instance == null)
            {
                Instance = new FormAuthorManagement();
            }
            return Instance;
        }

        private BookManagementEntities bookmanagementEntities = new BookManagementEntities();

        private void FillData()
        {
            this.dataGridView1.DataSource = bookmanagementEntities.Authors.Select(authors => new
            {
                id = authors.id,
                name = authors.name
            }).OrderByDescending(author => author.id).ToList();

        }

        private void Delete()
        {
            int authorId = int.Parse(this.dataGridView1.CurrentRow.Cells[0].Value.ToString());
            DialogResult dialogResult = MessageBox.Show("Are you sure", "Confirm", MessageBoxButtons.YesNo);
            if (dialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    Author author = bookmanagementEntities.Authors.Single(authors => authors.id.Equals(authorId));
                    bookmanagementEntities.Authors.Remove(author);
                    bookmanagementEntities.SaveChanges();
                    this.txtAuthorName.Clear();
                    MessageBox.Show("Deleted");
                }
                catch(Exception)
                {
                    MessageBox.Show("Fail");
                    this.txtAuthorName.Clear();
                }
            }
            FormBookManagement.GetInstance().load_author_grid();
        }

        private void FormAuthorManagement_Load(object sender, EventArgs e)
        {
            FillData();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int authorId = int.Parse(this.dataGridView1.CurrentRow.Cells[0].Value.ToString());
            Author authors = bookmanagementEntities.Authors.Single(author => author.id == authorId);
            this.txtAuthorName.Text = authors.name;
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            Delete();
            FillData();
            FormBookManagement.GetInstance().load_author_grid();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            bookmanagementEntities = new BookManagementEntities();
            if (txtAuthorName.Text == null)
                MessageBox.Show("Enter name please");
            else
            {
                try
                {
                    int authorId = int.Parse(this.dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    Author author = bookmanagementEntities.Authors.Where(a => a.id == authorId).SingleOrDefault();
                    author.name = this.txtAuthorName.Text;
                    bookmanagementEntities.SaveChanges();
                    FillData();
                    this.txtAuthorName.Clear();
                    MessageBox.Show("Update success");
                }
                catch
                {
                    MessageBox.Show("Update Fail");
                }
            }
            FormBookManagement.GetInstance().load_author_grid();
        }


        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            bookmanagementEntities = new BookManagementEntities();
            if (txtAuthorName.Text == "")
                MessageBox.Show("Enter name please");
            else
            {
                try
                {
                    Author author = new Author();
                    author.name = this.txtAuthorName.Text;
                    bookmanagementEntities.Authors.Add(author);
                    bookmanagementEntities.SaveChanges();
                    FillData();
                    this.txtAuthorName.Clear();
                    MessageBox.Show("Insert success");
                }
                catch
                {
                    MessageBox.Show("Insert fail");
                }
            }
            FormBookManagement.GetInstance().load_author_grid();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {

            FormBookManagement.GetInstance().Show();
            this.Hide();
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bookmanagementEntities.Authors.Where(au => au.name.Contains(txtAuthorName.Text)).Select(p => new
            {
                id = p.id,
                name = p.name
            }).ToList();
        }

        private void txtAuthorName_TextChanged(object sender, EventArgs e)
        {
            if (txtAuthorName.Text == "")
            {
                FillData();
            }
        }
    }
}