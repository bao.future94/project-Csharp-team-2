﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class FormBookUpdate : Form
    {
        BookManagementEntities bookEntities = new BookManagementEntities();
        int selectedRow;

        public FormBookUpdate()
        {
            InitializeComponent();
            this.isbntxtBox.ReadOnly = true;
            this.load_grid_Author();
            this.load_grid_Books();
            this.load_publisher_combobox();
            //this.button1.Text = "Back";
        }

        private void load_grid_Author()
        {
            var result = from Author in this.bookEntities.Authors
                         select new
                         {
                             ID = Author.id,
                             Name = Author.name
                         };
            dataGridView2.DataSource = result.ToList();
        }

        private void load_grid_Books()
        {
            var result = from Book in bookEntities.Books
                         from Author in Book.Authors
                         select new
                         {
                             ISBN = Book.isbn,
                             Title = Book.title,
                             Publisher = Book.Publisher.name,
                             Author = Author.name,
                             //Price = Book.price
                         };

            dataGridView1.DataSource = result.ToList();
        }

        private void load_publisher_combobox()
        {
            List<Publisher> publisher = bookEntities.Publishers.ToList();
            this.cbPublishers.DataSource = publisher;
            cbPublishers.DisplayMember = "name";
            cbPublishers.ValueMember = "id";
        }

        private int CheckUnfilledAndExistingData()
        {
            int result = 0;

            if (this.isbntxtBox.Text.Equals(""))
                result = 1;
            else if (this.titleTextBox.Text.Equals(""))
                result = 2;

            return result;
        }

        private Book RemoveAuthorsInBook(String id)
        {
            Book b = this.bookEntities.Books.Find(id);

            // remove book's authors
            if (b != null)
            {
                foreach (var author in b.Authors.ToList())
                {
                    if (!b.Authors.Remove(author))
                    {
                        b = null;
                        break;
                    }
                }
            }

            return b;
        }

        private Book updateBookInfo(String isbn, Book b)
        {
            Book temp = this.RemoveAuthorsInBook(isbn);
            if (temp != null)
            {
                //temp.price = b.price;
                temp.publisher_id = b.publisher_id;
                temp.Publisher = b.Publisher;
                temp.title = b.title;

                foreach (DataGridViewRow row in this.dataGridView2.SelectedRows)
                {
                    Author a = this.bookEntities.Authors.Find(int.Parse(row.Cells[0].Value.ToString()));
                    temp.Authors.Add(a);
                }

                bookEntities.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                bookEntities.SaveChanges();
            }

            return temp;
        }
        private void editBttn_Click(object sender, EventArgs e)
        {
            int result = this.CheckUnfilledAndExistingData();

            if (result == 1)
            {
                MessageBox.Show("You have to fill book's isbn !");
                return;
            }
            else if (result == 2)
            {
                MessageBox.Show("You have to fill book's title !");
                return;
            }

            Book updatedBook = new Book();
            updatedBook.isbn = this.isbntxtBox.Text;
            //updatedBook.price = decimal.Parse(this.priceTxtBox.Text);
            updatedBook.title = this.titleTextBox.Text;
            updatedBook.publisher_id = (int)this.cbPublishers.SelectedValue;
            updatedBook.Publisher = this.bookEntities.Publishers.Find(updatedBook.publisher_id);

            Book book = this.updateBookInfo(updatedBook.isbn, updatedBook);
            if (book == null)
            {
                
                MessageBox.Show("Updated failed");
                return;
            }

            if (book.Authors.Count == 0)
            {
                MessageBox.Show("You must choose authors");
                return;
            }

            this.load_grid_Books();
            FormHireBook.GetInstance().FillData();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            this.selectedRow = dataGridView1.CurrentRow.Index;
            this.isbntxtBox.Text = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
            this.titleTextBox.Text = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();
            //this.priceTxtBox.Text = this.dataGridView1.CurrentRow.Cells[4].Value.ToString();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            FormBookManagement.GetInstance().Show();            
            this.Hide();
        }

        private void FormBookUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
            this.Close();
        }
    }
}
