﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class FormHireBook : Form
    {
        private Order order;
        private static FormHireBook Instance = null;
        BookManagementEntities bme = new BookManagementEntities();
        List<Book> listBookSelected = new List<Book>();
        private FormHireBook()
        {
            InitializeComponent();
        }

        public static FormHireBook GetInstance()
        {
            if (Instance == null)
            {
                Instance = new FormHireBook();
            }
            return Instance;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            monthCalendar1.Show();
            //DateTime a = DateTime.Now;
            //MessageBox.Show(a.ToShortDateString());
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void FillComboBox()
        {
            List<Account> accounts = bme.Accounts.Where(a => a.role.Equals("ROLE_USER")).ToList();
            if (accounts.Count > 0)
            {
                comboBox1.DataSource = accounts;
                comboBox1.DisplayMember = "username";
            }
        }
        public void FillData()
        {
            var bookRecord = from Book in bme.Books
                             from Author in Book.Authors
                             select new
                             {
                                 ISBN = Book.isbn,
                                 Title = Book.title,
                                 Publisher = Book.Publisher.name,
                                 Author = Author.name
                             };

            dataGridView1.DataSource = bookRecord.ToList();
        }

        public FormHireBook LoadData()
        {
            dataGridView2.DataSource = null;
            listBookSelected.Clear();
            textBox1.Clear();
            label4.Text = "";
            return this;
        }

        public FormHireBook LoadComboBox()
        {
            FillComboBox();
            return this;
        }
        private void FillGridBookSelected()
        {
            dataGridView2.DataSource = null;
            if (listBookSelected.Count > 0)
            {
                dataGridView2.DataSource = listBookSelected;
            }

        }



        private void AutoSizeDgv2()
        {
            if (listBookSelected.Count > 0)
            {
                dataGridView2.Columns[2].Visible = false;
                dataGridView2.Columns[3].Visible = false;
                dataGridView2.Columns[4].Visible = false;
                dataGridView2.Columns[5].Visible = false;
            }
        }


        private void FormHireBook_Load(object sender, EventArgs e)
        {

            FillData();
            FillComboBox();


        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            //MessageBox.Show();

            label4.Text = monthCalendar1.SelectionRange.Start.ToShortDateString();
            monthCalendar1.Hide();


        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private bool IsExist(Book book)
        {
            foreach (Book b in listBookSelected)
            {
                if (book.isbn.Equals(b.isbn))
                {
                    return true;
                }
            }
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string isbn = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();



            Book books = bme.Books.Single(book => book.isbn.Equals(isbn));
            if (books != null && !IsExist(books))
            {

                listBookSelected.Add(books);
            }

            //foreach (Book book in listBookSelected)
            //{
            //    MessageBox.Show(book.title);
            //}
            // listBookSelected.ForEach(books => MessageBox.Show(books.title));

            FillGridBookSelected();
            AutoSizeDgv2();



        }

        private void button4_Click(object sender, EventArgs e)
        {
            string isbn = this.dataGridView2.CurrentRow.Cells[1].Value.ToString();
            Book books = bme.Books.Single(book => book.isbn.Equals(isbn));



            if (books != null)
            {
                listBookSelected.Remove(books);

            }
            FillGridBookSelected();
            AutoSizeDgv2();
        }

        //SAVVE button
        private void button5_Click(object sender, EventArgs e)
        {

            Account account = (Account)comboBox1.SelectedItem;
            string username = account.username;
            //MessageBox.Show(a.ToShortDateString());
            string title = textBox1.Text;
            DateTime createddate = DateTime.Now;
            DateTime returndate = monthCalendar1.SelectionRange.Start;
            Order order = new Order();
            if (!textBox1.Text.Equals("") && listBookSelected.Count > 0)
            {
                foreach (Book book in listBookSelected.ToList())
                {
                    //MessageBox.Show(book.title);
                    order.title = title;
                    order.createddate = createddate;
                    order.returndate = returndate;
                    order.username = username;
                    order.Books = listBookSelected;
                    order.Books.Add(book);

                }
                bme.Orders.Add(order);
                bme.SaveChanges();
                OpenFormOrderDetails();
                HideForm();
            }
            else
                MessageBox.Show("Not blank");
        }

        private void HideForm()
        {
            this.Hide();

        }

        private void OpenFormOrderDetails()
        {
            FormOrderDetails.GetInstance().LoadData().Show();

        }

        public FormHireBook Edit(Order order)
        {
            this.order = order;
            //hide button save and show button update
            //show information to input
            //show old book
            return this;
        }

        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAuthorManagement.GetInstance().Show();
        }

        private void publisherToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPublisherManagement.GetInstance().Show();
        }

        private void accountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAccountManagement.GetInstance().Show();
        }

        private void bookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormBookManagement.GetInstance().Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
