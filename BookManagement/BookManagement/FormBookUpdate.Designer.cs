﻿namespace BookManagement
{
    partial class FormBookUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBookUpdate));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.isbntxtBox = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPublishers = new System.Windows.Forms.ComboBox();
            this.labelPublisher = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.editBttn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(25, 215);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(485, 136);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // isbntxtBox
            // 
            this.isbntxtBox.Location = new System.Drawing.Point(111, 46);
            this.isbntxtBox.Name = "isbntxtBox";
            this.isbntxtBox.Size = new System.Drawing.Size(121, 20);
            this.isbntxtBox.TabIndex = 1;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(22, 49);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(32, 13);
            this.label.TabIndex = 2;
            this.label.Text = "ISBN";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(111, 96);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(121, 20);
            this.titleTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Title";
            // 
            // cbPublishers
            // 
            this.cbPublishers.FormattingEnabled = true;
            this.cbPublishers.Location = new System.Drawing.Point(111, 142);
            this.cbPublishers.Name = "cbPublishers";
            this.cbPublishers.Size = new System.Drawing.Size(121, 21);
            this.cbPublishers.TabIndex = 7;
            // 
            // labelPublisher
            // 
            this.labelPublisher.AutoSize = true;
            this.labelPublisher.Location = new System.Drawing.Point(22, 150);
            this.labelPublisher.Name = "labelPublisher";
            this.labelPublisher.Size = new System.Drawing.Size(50, 13);
            this.labelPublisher.TabIndex = 8;
            this.labelPublisher.Text = "Publisher";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(251, 46);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(259, 117);
            this.dataGridView2.TabIndex = 9;
            // 
            // editBttn
            // 
            this.editBttn.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.editBttn.FlatAppearance.BorderSize = 0;
            this.editBttn.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.editBttn.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.editBttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editBttn.Image = ((System.Drawing.Image)(resources.GetObject("editBttn.Image")));
            this.editBttn.Location = new System.Drawing.Point(417, 169);
            this.editBttn.Name = "editBttn";
            this.editBttn.Size = new System.Drawing.Size(75, 40);
            this.editBttn.TabIndex = 10;
            this.editBttn.UseVisualStyleBackColor = true;
            this.editBttn.Click += new System.EventHandler(this.editBttn_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(423, 357);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 36);
            this.button1.TabIndex = 11;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Back_Click);
            // 
            // FormBookUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 405);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.editBttn);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.labelPublisher);
            this.Controls.Add(this.cbPublishers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(this.label);
            this.Controls.Add(this.isbntxtBox);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.Name = "FormBookUpdate";
            this.Text = "Book Update";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormBookUpdate_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox isbntxtBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbPublishers;
        private System.Windows.Forms.Label labelPublisher;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button editBttn;
        private System.Windows.Forms.Button button1;
    }
}